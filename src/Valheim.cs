using Steamworks;
using System;
using System.Collections.Generic;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;
using UnityEngine;

namespace uMod.Game.Valheim
{
    /// <summary>
    /// The core Valheim plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Valheim : Plugin
    {
        #region Initialization

        internal static readonly ValheimProvider Universal = ValheimProvider.Instance;

        private bool serverInitialized;

        /// <summary>
        /// Initializes a new instance of the Valheim class
        /// </summary>
        public Valheim()
        {
            // Set plugin info attributes
            Title = "Valheim";
            Author = ValheimExtension.AssemblyAuthors;
            Version = ValheimExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();

            serverInitialized = true;

            // Register RPC for VChat mod
            ZRoutedRpc.instance.Register("org.itskaa.vchat.greet", new RoutedMethod<string>(OnVChatGreetingReceived).m_action);
        }

        #endregion Initialization

        #region Server Hooks

        /// <summary>
        /// Called when a server command was run
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(string playerId, string command, string[] args)
        {
            IPlayer player = Players.FindPlayerById(playerId);
            if (player != null)
            {
                // Is the player command blocked?
                if (Universal.CommandSystem.HandleConsoleMessage(player, $"{command} {string.Join(" ", args)}") == CommandState.Completed)
                {
                    return true;
                }

                return null;
            }

            // Is the server command blocked?
            if (Interface.CallHook("OnServerCommand", command, args) != null)
            {
                return true;
            }

            return null;
        }

        // Players using the VChat client mod
        private readonly Dictionary<long, string> _VChatUsers = new Dictionary<long, string>();

        /// <summary>
        /// Called when a VChat mod-enabled client sends a greeting
        /// </summary>
        /// <param name="senderId"></param>
        /// <param name="vChatVersion"></param>
        private void OnVChatGreetingReceived(long senderId, string vChatVersion)
        {
            ZNetPeer zNetPeer = ZRoutedRpc.instance.GetPeer(senderId);
            if (zNetPeer != null && zNetPeer.IsReady())
            {
                Logger.Info($"Greeting received from client {zNetPeer.m_playerName} ({senderId}) with version {vChatVersion}");
                if (_VChatUsers.ContainsKey(senderId))
                {
                    _VChatUsers[senderId] = vChatVersion;
                }
                else
                {
                    _VChatUsers.Add(senderId, vChatVersion);
                }
            }
        }

        /// <summary>
        /// Called when the server adds a new peer
        /// </summary>
        /// <param name="zNetPeer"></param>
        [Hook("IOnAddPeer")]
        private void IOnAddPeer(ZNetPeer zNetPeer)
        {
            // Tell the client that the server is VChat mod compatible
            ZRoutedRpc.instance.InvokeRoutedRPC(zNetPeer.m_uid, "org.itskaa.vchat.greet", "uMod");
        }

        /// <summary>
        /// Called when the server receives routed RPC data
        /// </summary>
        /// <param name="rpcData"></param>
        [Hook("IOnHandleRoutedRPC")]
        private object IOnHandleRoutedRPC(ZRoutedRpc.RoutedRPCData rpcData)
        {
            ZNetPeer zNetPeer = ZNet.instance.GetPeer(rpcData.m_senderPeerID);
            bool isShout = rpcData.m_methodHash == "ChatMessage".GetStableHashCode();
            bool isNormalOrWhisper = rpcData.m_methodHash == "Say".GetStableHashCode();
            bool isVChatGlobal = rpcData.m_methodHash == "org.itskaa.vchat.globalchat".GetStableHashCode();

            // Handle RPC data for chat messages
            if (zNetPeer != null && !zNetPeer.m_server && (isShout || isNormalOrWhisper || isVChatGlobal))
            {
                ZPackage zPackage = new ZPackage(rpcData.m_parameters.GetArray());
                Vector3 position = isShout ? zPackage.ReadVector3() : zNetPeer.m_refPos;
                int talkerType = zPackage.ReadInt();
                string senderName = zPackage.ReadString();
                string message = zPackage.ReadString().Trim();

                if (!string.IsNullOrEmpty(message))
                {
                    // Is the chat message a command?
                    if (Interface.uMod.Plugins.Configuration.Commands.ChatCommandPrefixes.Contains(message[0]))
                    {
                        return Universal.CommandSystem.HandleChatMessage(zNetPeer.IPlayer, message);
                    }

                    // Call hooks for plugins
                    object chatUniversal = Interface.CallHook("OnPlayerChat", zNetPeer, message, talkerType, position);
                    object chatDeprecated = Interface.CallDeprecatedHook("OnUserChat", "OnPlayerChat", new DateTime(2022, 1, 1), zNetPeer.IPlayer, message);
                    object canChat = chatUniversal is null ? chatDeprecated : chatUniversal;

                    // Is the chat message blocked?
                    if (canChat != null)
                    {
                        return true;
                    }

                    // Log chat output
                    string channel = isVChatGlobal ? "Global" : ((Talker.Type)talkerType).ToString();
                    Logger.Info($"[Chat] [{channel}] {senderName} said: {message}");

                    if (isVChatGlobal)
                    {
                        foreach (ZNetPeer target in ZNet.instance.GetConnectedPeers())
                        {
                            // Ignore if target is not a valid target
                            if (target == null || target.m_server || !target.IsReady())
                            {
                                continue;
                            }

                            // Ignore if target is sender
                            if (target.m_uid == rpcData.m_senderPeerID)
                            {
                                continue;
                            }

                            // Check if target is VChat client mod user
                            if (_VChatUsers.ContainsKey(target.m_uid))
                            {
                                // Sent as global chat if a VChat client mod user
                                ZRoutedRpc.instance.InvokeRoutedRPC(target.m_uid, "org.itskaa.vchat.globalchat", zNetPeer.m_refPos, 100, senderName, message);
                            }
                            else
                            {
                                // Sent as local chat if not a VChat client mod user
                                ZRoutedRpc.instance.InvokeRoutedRPC(target.m_uid, "ChatMessage", zNetPeer.m_refPos, 1, "[Global]", string.Concat(senderName, ": ", message));
                            }
                        }

                        rpcData.m_methodHash = "org.itskaa.vchat.globalchat".GetStableHashCode();
                        return true;
                    }
                }
            }

            return null;
        }

        #endregion Server Hooks

        #region Player Hooks

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="zNetPeer"></param>
        /// <returns></returns>
        [Hook("IOnPlayerApprove")]
        private object IOnPlayerApprove(ZNetPeer zNetPeer)
        {
            if (!serverInitialized)
            {
                // Reject player connection
                ZNet.instance.SendDisconnect(zNetPeer);
                ZNet.instance.Disconnect(zNetPeer);
                return true;
            }

            string playerName = zNetPeer.m_playerName;
            string playerId = zNetPeer.m_socket.GetHostName();
            SteamGameServerNetworkingSockets.GetConnectionInfo(((ZSteamSocket)zNetPeer.m_socket).m_con, out SteamNetConnectionInfo_t steamNetConnectionInfo);
            uint ip = steamNetConnectionInfo.m_addrRemote.GetIPv4();
            string ipAddress = string.Concat(ip >> 24 & 255, ".", ip >> 16 & 255, ".", ip >> 8 & 255, ".", ip & 255);

            Players.PlayerJoin(playerId, playerName);
            IPlayer player = Players.FindPlayerById(playerId);
            if (player != null)
            {
                // Set IPlayer object in ZNetPeer
                zNetPeer.IPlayer = player;
            }

            if (Interface.uMod.Auth.Configuration.Groups.AutomaticGrouping)
            {
                // Add player to default groups
                GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
                if (!permission.UserHasGroup(playerId, defaultGroups.Players))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Players);
                }
                if (ZNet.instance.m_adminList.Contains(playerId) && !permission.UserHasGroup(playerId, defaultGroups.Administrators))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Administrators);
                }
            }

            // Call pre hook for plugins
            object loginUniversal = Interface.CallHook("CanPlayerLogin", playerName, playerId, ipAddress);
            object loginDeprecated = Interface.CallDeprecatedHook("CanUserLogin", "CanPlayerLogin(string playerName, string playerId, string ipAddress)",
                new DateTime(2022, 1, 1), playerName, playerId, ipAddress);
            object canLogin = loginUniversal is null ? loginDeprecated : loginUniversal;

            // Can the player log in?
            if (canLogin is string || canLogin is bool loginBlocked && !loginBlocked)
            {
                if (canLogin is string && !string.IsNullOrEmpty(canLogin as string))
                {
                    ZNet.instance.RemotePrint(zNetPeer.m_rpc, canLogin as string);
                }

                // Reject player connection
                ZNet.instance.SendDisconnect(zNetPeer);
                ZNet.instance.Disconnect(zNetPeer);
                return true;
            }

            // Call post hook for plugins
            Interface.CallHook("OnPlayerApproved", playerName, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApprove", "OnPlayerApproved(string playerName, string playerId, string ipAddress)",
                new DateTime(2022, 1, 1), playerName, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved(string playerName, string playerId, string ipAddress)",
                new DateTime(2022, 1, 1), playerName, playerId, ipAddress);

            return null;
        }

        /// <summary>
        /// Called when the player sends their character ID
        /// </summary>
        /// <param name="zNetPeer"></param>
        [Hook("IOnPlayerCharacterId")]
        private void IOnPlayerCharacterId(ZNetPeer zNetPeer)
        {
            string playerId = zNetPeer.m_socket.GetHostName();

            Players.PlayerConnected(playerId, zNetPeer);

            // Check if player just connected
            if (!zNetPeer.IPlayer.HasSpawnedBefore)
            {
                timer.Once(3f, () =>
                {
                    // Call hook for plugins
                    Interface.CallHook("OnPlayerConnected", zNetPeer.IPlayer);

                    // Call deprecated hook for plugins
                    Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2022, 1, 1), zNetPeer.IPlayer);

                    zNetPeer.IPlayer.HasSpawnedBefore = true;
                    zNetPeer.IPlayer.IsReturningPlayer = true;
                });
                return;
            }

            // Check if player died and respawned
            if (zNetPeer.m_characterID.userID == 0L)
            {
                Interface.CallHook("OnPlayerDeath", zNetPeer.IPlayer); // TODO: Return to cancel when able
                Interface.CallHook("OnPlayerRespawn", zNetPeer.IPlayer); // TODO: Return to cancel when able
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="zNetPeer"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(ZNetPeer zNetPeer)
        {
            IPlayer player = zNetPeer?.IPlayer;
            if (player != null)
            {
                zNetPeer.IPlayer.HasSpawnedBefore = false;
                Players.PlayerDisconnected(player.Id);

                // Call hook for plugins
                Interface.CallHook("OnPlayerDisconnected", zNetPeer.IPlayer);

                // Call deprecated hook for plugins
                Interface.CallDeprecatedHook("OnUserDisconnected", "OnPlayerDisconnected", new DateTime(2022, 1, 1), player);
            }
            else if (zNetPeer?.m_socket is ZSteamSocket zSteamSocket)
            {
                Players.PlayerDisconnected(zSteamSocket.m_peerID.GetSteamID().ToString());
            }
        }

        #endregion Player Hooks
    }
}
