﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.Valheim
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class ValheimPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Valheim) };

        public ValheimPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
