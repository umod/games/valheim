﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.Valheim
{
    /// <summary>
    /// Represents a Valheim player manager
    /// </summary>
    public class ValheimPlayerManager : PlayerManager<ValheimPlayer>
    {
        /// <summary>
        /// Create a new instance of the ValheimPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public ValheimPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="valheimPlayer"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, ValheimPlayer valheimPlayer, int playerFilter = 0)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, valheimPlayer, playerFilter))
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Id) != 0 && valheimPlayer.zNetPeer != null && valheimPlayer.zNetPeer.IsReady())
            {
                return valheimPlayer.zNetPeer.m_uid.Equals(partialNameOrIdOrIp);
            }

            return false;
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, IPlayer player, int playerFilter = 0)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player, playerFilter))
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Id) != 0 && player is ValheimPlayer valheimPlayer && valheimPlayer.zNetPeer != null && valheimPlayer.zNetPeer.IsReady())
            {
                return valheimPlayer.zNetPeer.m_uid.Equals(partialNameOrIdOrIp);
            }

            return false;
        }
    }
}
