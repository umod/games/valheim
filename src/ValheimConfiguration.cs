﻿using uMod.IO;

namespace uMod.Game.Valheim
{
    public class ValheimConfiguration : TomlFile
    {
        /// <summary>
        /// Create a new instance of a server configuration
        /// </summary>
        /// <param name="filename"></param>
        public ValheimConfiguration(string filename) : base(filename)
        {
        }
    }
}
