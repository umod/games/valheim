using Steamworks;
using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.Valheim
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class ValheimPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        internal ZNetPeer zNetPeer;

        public ValheimPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
        }

        public ValheimPlayer(ZNetPeer zNetPeer) : this(zNetPeer.m_socket.GetHostName(), zNetPeer.m_playerName)
        {
            // Store player object
            this.zNetPeer = zNetPeer;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets/sets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            zNetPeer = gamePlayer as ZNetPeer;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultureInfo("en"); // TODO: Implement if possible

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address
        {
            get
            {
                if (IsConnected)
                {
                    ZSteamSocket zSteamSocket = (ZSteamSocket)zNetPeer.m_socket;
                    SteamGameServerNetworkingSockets.GetConnectionInfo(zSteamSocket.m_con, out SteamNetConnectionInfo_t steamNetConnectionInfo);
                    uint ip = steamNetConnectionInfo.m_addrRemote.GetIPv4();
                    return string.Concat(ip >> 24 & 255, ".", ip >> 16 & 255, ".", ip >> 8 & 255, ".", ip & 255);
                }

                return "Unknown"; // TODO: Localization
            }
        }

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => 0; // TODO: Implement

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => ZNet.instance.m_adminList.Contains(Id) || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Administrators);

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Moderators);

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => ZNet.instance.m_bannedList.Contains(Id);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => zNetPeer?.m_socket?.IsConnected() ?? false; // TODO: Test and verify

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => !IsDead;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead
        {
            get
            {
                if (IsConnected)
                {
                    ZDO zDo = ZNet.instance.m_zdoMan.GetZDO(zNetPeer.m_characterID);
                    return zDo.IsValid() && zDo.GetBool("dead");
                }

                return false;
            }
        }

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping
        {
            get
            {
                if (IsConnected)
                {
                    ZDO zDo = ZNet.instance.m_zdoMan.GetZDO(zNetPeer.m_characterID);
                    return zDo.IsValid() && zDo.GetBool("inBed");
                }

                return false;
            }
        }

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => zNetPeer.m_server;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban and kick player
                ZNet.instance.m_bannedList.Add(Address);
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the game
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                if (!string.IsNullOrEmpty(reason))
                {
                    ZNet.instance.RemotePrint(zNetPeer.m_rpc, reason); // TODO: Find a better way to show this?
                }

                ZNet.instance.SendDisconnect(zNetPeer);
                ZNet.instance.Disconnect(zNetPeer);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                ZNet.instance.m_bannedList.Remove(Address);
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get
            {
                if (IsConnected)
                {
                    ZDO zDo = ZNet.instance.m_zdoMan.GetZDO(zNetPeer.m_characterID);
                    return zDo.IsValid() ? zDo.GetFloat("health") : 0f; // TODO: Test and verify
                }

                return 0f;
            }

            set => throw new NotSupportedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get
            {
                if (IsConnected)
                {
                    ZDO zDo = ZNet.instance.m_zdoMan.GetZDO(zNetPeer.m_characterID);
                    return zDo.IsValid() ? zDo.GetFloat("max_health", 10f) : 0f; // TODO: Test and verify
                }

                return 0f;
            }

            set => throw new NotSupportedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (Health > 0f && !IsDead)
            {
                ZDO zDo = ZNet.instance.m_zdoMan.GetZDO(zNetPeer.m_characterID);
                if (zDo.IsValid() && zNetPeer.IsReady() && !zNetPeer.m_characterID.IsNone())
                {
                    ZRoutedRpc.instance.InvokeRoutedRPC(zDo.m_owner, zDo.m_uid, "Heal", Mathf.Min(Health + amount, MaxHealth), true);
                }
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            if (Health > 0f && !IsDead)
            {
                ZDO zDo = ZNet.instance.m_zdoMan.GetZDO(zNetPeer.m_characterID);
                if (zDo.IsValid() && zNetPeer.IsReady() && !zNetPeer.m_characterID.IsNone())
                {
                    HitData hitData = new HitData();
                    hitData.m_damage.m_damage = amount;
                    ZRoutedRpc.instance.InvokeRoutedRPC(zDo.m_owner, zDo.m_uid, "Damage", hitData);
                }
            }
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => Hurt(Health);

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            if (IsConnected)
            {
                ZDO zDo = ZNet.instance.m_zdoMan.GetZDO(zNetPeer.m_characterID);
                if (zDo.IsValid() && zNetPeer.IsReady() && !zNetPeer.m_characterID.IsNone())
                {
                    zDo.Set("playerName", newName); // TODO: Test and verify
                }
            }
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset() => throw new NotSupportedException(); // TODO: Implement when possible

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn() => throw new NotSupportedException(); // TODO: Implement when possible

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos) => throw new NotSupportedException(); // TODO: Implement when possible

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => zNetPeer?.m_refPos.ToPosition();

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            throw new NotSupportedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player as a chat message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                // RPC_ChatMessage(long sender, Vector3 position, int type, string name, string text)
                // Talker.Type/int type: Whisper = 0, Normal = 1, Shout = 2, Ping = 3
                message = args.Length > 0 ? string.Format(message, args) : message;
                ZRoutedRpc.instance.InvokeRoutedRPC(zNetPeer.m_uid, "ChatMessage", zNetPeer.m_refPos + new Vector3(0, 1.4f, 0), 1, prefix ?? string.Empty, message);
            }
        }

        /// <summary>
        /// Sends the specified message to the player as a chat message
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Sends the specified message to the player as a notice on screen
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public void Notice(string message, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                // RPC_ShowMessage(long sender, int type, string text)
                // MessageHud.MessageType/int type: TopLeft = 1, Center = 2
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                ZRoutedRpc.instance.InvokeRoutedRPC(zNetPeer.m_uid, "ShowMessage", 2, message);
            }
        }

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                throw new NotSupportedException(); // TODO: Implement if possible
            }
        }

        #endregion Chat and Commands
    }
}
