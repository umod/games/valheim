@echo off
cls
:start
echo Starting server...

valheim_server.exe -batchmode -nographics -name "My uMod Server" -port 2456 -world "Dedicated" -password "secret" -public 1

echo.
echo Restarting server...
timeout /t 10
echo.
goto start
